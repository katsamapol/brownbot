<?php // callback.php

require "vendor/autoload.php";
require_once('vendor/linecorp/line-bot-sdk/line-bot-sdk-tiny/LINEBotTiny.php');

$access_token = 'KG2xLmoMZZIgDmU1+53s0hCVnkrBauMkqfYyOZy7tLIga/w+k6QiYFTTQ1xcd1n+PpEhyDPHd3d91cOjTpkx2WKu6H74LmE1A2M9cMzQdIcpFwkk5W0qzbe/yp7LiZgkeoWK9LbksVaec0XTKz2+oAdB04t89/1O/w1cDnyilFU=';

// Get POST body content
$content = file_get_contents('php://input');
// Parse JSON
$events = json_decode($content, true);
// Validate parsed JSON data
if (!is_null($events['events'])) {
	// Loop through each event
	foreach ($events['events'] as $event) {
		$replyText = "";
		// Reply only when message sent is in 'text' format
		if ($event['type'] == 'message' && $event['message']['type'] == 'text') {
			// Get text sent
			//$replyText = $event['source']['userId'];
			$text = $event['message']['text'];
			if($text == 'แนทอายุเท่าไหร่') {
				$replyText = '39';
			}else if($text == 'Appearence'){
				$replyText = "Brown is a brown bear. He has small, black eyes (just the same as Cony's), two tiny ears and a short tail, he never shows his emotions through his face.";
			}else if($text == 'Personality'){
				$replyText = "Brown is silent, calm and quite shy bear but when he gets furious, he will punish his friends in a way even more frightening than Cony. Although he sometimes fights with Cony, he is still a caring and gentle spouse of hers.";
			}else if($text == 'Trivia'){
				$replyText = "Brown is sometimes shown dating Jessica or Sally. \r\n
He is one of the four original characters. \r\n
As he is a brown bear, Brown's mother is a white bear and his father is a black bear. \r\n
Brown is the only character to have a sister. her name is Choco. \r\n
In LINE TOWN, Brown's house is unknown. \r\n
In the manga series, Brown does have expressions. \r\n
Brown's birthday is actually the exact date of Father's Day.";
			}else if($text == 'Birthday'){
				$replyText = "8th August";
			}else if($text == 'Family'){
				$replyText = "Cony(wife), Choco(little sister), Uncle Brown(uncle).";
			}else if($text == 'Hobbies'){
				$replyText = "eating salmon, hanging out with Sally & dating with Cony";
			}else{
				$replyText = "";
			}
			if($replyText != ""){
				// Build message to reply back
				$messages = [
					'type' => 'text',
					'text' => $replyText
				];
			}
		}else if($event['type'] == 'message' && $event['message']['type'] == 'sticker'){
			$packageId = $event['message']['packageId'];
			$stickerId = $event['message']['stickerId'];

			// Build message to reply back
			$messages = [
				'type' => 'sticker',
				'packageId' => "2",
				'stickerId' => rand(501,514)
			];
		}

		if($messages != ""){
			// Get replyToken
			$replyToken = $event['replyToken'];

			// Make a POST Request to Messaging API to reply to sender
			$url = 'https://api.line.me/v2/bot/message/reply';
			$data = [
				'replyToken' => $replyToken,
				'messages' => [$messages],
			];
			$post = json_encode($data);
			$headers = array('Content-Type: application/json', 'Authorization: Bearer ' . $access_token);

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			$result = curl_exec($ch);
			curl_close($ch);

			echo $result . "\r\n";
		}
	}
}
echo "OK";